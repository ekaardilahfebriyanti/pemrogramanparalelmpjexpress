/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjExpress;

import mpi.MPI;

/**
 *
 * @author Eka Ardilah FebriY
 */
public class SendReceive1 {
    public static void main(String[] args) {
        MPI.Init(args);
        int size = MPI.COMM_WORLD.Size();
        int rank = MPI.COMM_WORLD.Rank();

        //data harus berbentuk array di MPJ
        int data[]={0,1,2,3,4,5,6,7,8,9};
        char dataChar[] = {'e','k','a'};
        String pesan []= {"Latihan pemrograman Paralel"};
        //perintah send
        if(rank==0){
            //parameter berisi data yang akan dikirim, offset (jika tidak diatur bisa dimulai dari 0 jika tidak ditentukan
            //panjang data, lalu tipe data, kemudian destinasi, lali tag nya

            for (int i = 1; i < size; i++) {
                 MPI.COMM_WORLD.Send(data, 0, data.length, MPI.INT, i, 1);
                MPI.COMM_WORLD.Send(dataChar, 0, dataChar.length, MPI.CHAR, i, 0); 
                MPI.COMM_WORLD.Send(pesan, 0, pesan.length, MPI.OBJECT, i, 2);
            }
        }
        //perintah received, pilih rank mana yang menerima
        else{
            //jika ingin melihat perbedaan maka kita tetapkan tempat untuk data penampung
            int dataRecv [] = new int[10];
            char dataCharRecv[] = new char[3];
            String pesanRecv[] = new String[1];
            MPI.COMM_WORLD.Recv(dataRecv, 0, dataRecv.length, MPI.INT, 0, 1);
            MPI.COMM_WORLD.Recv(dataCharRecv, 0, dataCharRecv.length, MPI.CHAR, 0, 0);
            MPI.COMM_WORLD.Recv(pesanRecv, 0, pesanRecv.length, MPI.OBJECT, 0, 2);
            
            
            //tampilkan isi data
            System.out.println("data dari prosesor : "+rank);
            for(int i=0; i<dataCharRecv.length;i++){
                System.out.print(dataCharRecv[i]+" ");
            }
            System.out.println("");
            
            for(int i=0;i< dataRecv.length;i++){
                System.out.print(dataRecv[i]);
            }
            System.out.println("");
            for (int i = 0; i < pesanRecv.length; i++) {
                System.out.println(pesanRecv[i]);
            }
            System.out.println("");
        }
        MPI.Finalize();
    }
}
