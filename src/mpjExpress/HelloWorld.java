/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjExpress;

import mpi.MPI;

/**
 *
 * @author Eka Ardilah FebriY
 */
public class HelloWorld {
    public static void main(String[] args) {
        MPI.Init(args);
        int me = MPI.COMM_WORLD.Rank();
        int size = MPI.COMM_WORLD.Size();
        System.out.println("Hi From <"+me+">");
        MPI.Finalize();
    }
}
