/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mpjExpress;

import mpi.MPI;

/**
 *
 * @author Eka Ardilah FebriY
 */
public class SendReceive {
    public static void main(String[] args) {
        MPI.Init(args);
        int size = MPI.COMM_WORLD.Size();
        int rank = MPI.COMM_WORLD.Rank();
        
        
        System.out.println("Hello from "+rank);
        //data harus berbentuk array di MPJ
        int data[]={1,2,3,4,5,6,7,8};
        char dataChar[] = {'e','k','a'};
        //perintah send
        if(rank==0){
            //parameter berisi data yang akan dikirim, offset (jika tidak diatur bisa dimulai dari 0 jika tidak ditentukan
            //panjang data, lalu tipe data, kemudian destinasi, lali tag nya
            MPI.COMM_WORLD.Send(data, 0, data.length, MPI.INT, 1, 0);
            MPI.COMM_WORLD.Send(dataChar, 0, dataChar.length, MPI.CHAR, 1, 0);
        }
        //perintah received, pilih rank mana yang menerima
        if(rank==1){
            //jika ingin melihat perbedaan maka kita tetapkan tempat untuk data penampung
            int dataRecv [] = new int[8];
            char dataCharRecv[] = new char[3];
            MPI.COMM_WORLD.Recv(dataRecv, 0, dataRecv.length, MPI.INT, 0, 0);
            MPI.COMM_WORLD.Recv(dataCharRecv, 0, dataCharRecv.length, MPI.CHAR, 0, 0);
            
            
            //tampilkan isi data
            for(int i=0; i<dataCharRecv.length;i++){
                System.out.println(dataCharRecv[i]);
            }
            for(int i=0;i< dataRecv.length;i++){
                System.out.println(dataRecv[i]);
            }
        }
        MPI.Finalize();
    }
}
